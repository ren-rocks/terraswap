package main

import (
	"os"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"github.com/tidwall/gjson"
)

var x = omdb{}

func TestGetRandomMovie(t *testing.T) {
	x.setupKey()

	t.Run("omdb id is valid", func(t *testing.T) {
		x.getValidMovieID()
		var re = regexp.MustCompile(`(?s)^tt[0-9]{7}$`)
		if len(re.FindStringIndex(x.randomID)) <= 0 {
			t.Errorf("%s does not start with \"tt\"", x.randomID)
		}

		i, err := strconv.Atoi(strings.Replace(x.randomID, "tt", "", -1))
		if err != nil {
			t.Errorf("could not convert %s into an in after stripping \"tt\".", x.randomID)
		}

		if i < 0 || i > 2290000 {
			t.Errorf("%s should be between 0 and 2290000", x.randomID)
		}
	})

	t.Run("omdb api returns a valid json", func(t *testing.T) {
		err := x.getImdbData()
		if err != nil {
			t.Errorf("getImdbData should not throw an error: %s", err)
		}

		if x.imdbJSON == "" && !gjson.Valid(x.imdbJSON) {
			t.Errorf("%s should be a valid JSON string", x.imdbJSON)
		}
	})

	t.Run("omdb api returns the correct movie title", func(t *testing.T) {
		x.randomID = "tt1489887"
		_ = x.getImdbData()
		err := x.getMovieTitle()
		if err != nil {
			t.Errorf("getMovieTitle should not throw an error: %s", err)
		}

		if x.imdbTitle != "Booksmart" {
			t.Errorf("%s should be a valid imdb movie title", x.imdbTitle)
		}
	})
}

func TestSwapText(t *testing.T) {
	t.Run("processSentence should throw an error if the content.sentece is \"\"", func(t *testing.T) {
		var c content
		err := c.processSentence()
		if err == nil {
			t.Errorf("processSentence should not throw an error: %s", err)
		}
	})

	t.Run("processSentence should split a movie title on a vowel \"\"", func(t *testing.T) {
		var c content
		c.sentence = "Booksmart"
		_ = c.processSentence()
		if c.output != "Terraoksmart" {
			t.Errorf("processSentence should return Terraoksmart if Booksmart is the sentence input. c.output returns %s", c.output)
		}
	})

	t.Run("processSentence should swap a noun for Terraform \"\"", func(t *testing.T) {
		var c content
		c.sentence = "Journey to the center of the Earth"
		_ = c.processSentence()
		if c.output != "Terraform to the center of the Earth" {
			t.Errorf("processSentence should return \"Terraform to the center of the Earth\". c.output returns %s", c.output)
		}
	})

	t.Run("processSentence should swap a verb for Terraforming if a noun is not present \"\"", func(t *testing.T) {
		var c content
		c.sentence = "This is Large!"
		_ = c.processSentence()
		if c.output != "This is Terraforming!" {
			t.Errorf("processSentence should return \"This is Terraforming!\". c.output returns %s", c.output)
		}
	})
}

var m = img{randomID: "tt1489887"}

func TestBuildPoster(t *testing.T) {
	m.setupKey()

	t.Run("omdb poster img is downloaded successfully", func(t *testing.T) {
		err := m.downloadPoster()
		if err != nil {
			t.Error(err)
		}

		if _, err := os.Stat(m.tmpPath); os.IsNotExist(err) {
			t.Errorf("could not find tmp file %s", m.tmpPath)
		}
	})

	t.Run("omdb poster img is blurred successfully", func(t *testing.T) {
		err := m.blurImage()
		if err != nil {
			t.Error(err)
		}

		if _, err := os.Stat(m.tmpPath); os.IsNotExist(err) {
			t.Errorf("could not find tmp file %s", m.tmpPath)
		}
	})

	t.Run("create new image from text watermark and poster", func(t *testing.T) {
		err := m.mergeImages("test")
		if err != nil {
			t.Error(err)
		}

		if _, err := os.Stat(m.tmpPath); os.IsNotExist(err) {
			t.Errorf("could not find tmp file %s", m.tmpPath)
		}

		if _, err := os.Stat(m.outputPath); os.IsNotExist(err) {
			t.Errorf("could not find output file %s", m.tmpPath)
		}
	})

}
