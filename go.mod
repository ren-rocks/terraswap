module gitlab.com/ren-rocks/terraswap

go 1.14

require (
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/fogleman/gg v1.3.0
	github.com/google/uuid v1.1.1
	github.com/mingrammer/commonregex v1.0.1 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/noelyahan/impexp v0.0.0-20190507042629-ba8d234c5451
	github.com/noelyahan/mergi v0.0.0-20190514155713-df743a5b4419
	github.com/tidwall/gjson v1.6.0
	github.com/tidwall/pretty v1.0.1 // indirect
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8 // indirect
	gonum.org/v1/gonum v0.7.0 // indirect
	gopkg.in/jdkato/prose.v2 v2.0.0-20190814032740-822d591a158c
	gopkg.in/neurosnap/sentences.v1 v1.0.6 // indirect
)
