package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/disintegration/imaging"
	"github.com/fogleman/gg"
	"github.com/google/uuid"
	"github.com/noelyahan/impexp"
	"github.com/noelyahan/mergi"
	"github.com/tidwall/gjson"
	"gopkg.in/jdkato/prose.v2"
)

var (
	o = flag.String("o", "", "")
	i = flag.Bool("i", false, "")
)

var usage = `Usage: terraswap [options...]
creates an image (default) or outputs a text of a random movie title
with a noun swapped for the word "terraform"

Options:
	-o	output path for your image (defaults to $HOME directory)
	-i	outputs image file instead of text
`

func main() {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, fmt.Sprint(usage))
	}
	flag.Parse()

	runTerraswap()
}

func runTerraswap() {
	var err error
	var movieTitle string

	defer func() {
		if err != nil {
			fmt.Printf("error running terraswap: %v. \n", err)
			os.Exit(1)
		}
	}()

	movieTitle, randomID, err := RunTerraswapText()
	if err != nil {
		return
	}

	if *i == false {
		fmt.Printf("%s - https://imdb.com/title/%s \n", movieTitle, randomID)
		return
	}

	var m = img{randomID: randomID}
	m.setupKey()

	err = m.downloadPoster()
	if err != nil {
		return
	}

	err = m.blurImage()
	if err != nil {
		return
	}

	err = m.mergeImages(movieTitle)
	if err != nil {
		return
	}

	fmt.Printf("successfully created image %s\n", m.outputPath)
}

// RunTerraswapText outputs a terraform movie title text
func RunTerraswapText() (string, string, error) {
	var err error

	o := omdb{}
	o.setupKey()
	o.getValidMovieID()

	err = o.getImdbData()
	if err != nil {
		return "", "", err
	}

	err = o.getMovieTitle()
	if err != nil {
		return "", "", err
	}

	var c content
	c.sentence = o.imdbTitle
	err = c.processSentence()

	if err != nil {
		return "", "", err
	}

	return c.output, o.randomID, err
}

type omdb struct {
	omdbKey    string
	randomID   string
	omdbStatus string
	imdbJSON   string
	imdbTitle  string
}

func (o *omdb) setupKey() {
	omdbKey, found := os.LookupEnv("OMDB_KEY")
	if found != true {
		log.Fatal("OMDB_KEY not found as an env variable")
	}

	o.omdbKey = omdbKey
}

func (o *omdb) getValidMovieID() {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	o.randomID = fmt.Sprintf("tt%07d", r1.Intn(2290000))
}

func (o *omdb) getImdbData() error {
	omdbURL := fmt.Sprintf("http://www.omdbapi.com/?apikey=%s&i=%s", o.omdbKey, o.randomID)
	resp, err := http.Get(omdbURL)

	switch true {
	case err != nil:
		return err
	case resp.StatusCode != 200:
		return fmt.Errorf("omdb API call does not return a 200.\nURL: %s\nStatus: %d", omdbURL, resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	o.imdbJSON = string(body)
	return nil
}

func (o *omdb) getMovieTitle() error {
	gjsonErr := gjson.Get(o.imdbJSON, "Error").String()

	if gjsonErr != "" {
		return fmt.Errorf("omdb API call returned an error: %s", gjsonErr)
	}

	o.imdbTitle = gjson.Get(o.imdbJSON, "Title").String()
	return nil
}

func inArray(needle string, haystack []string) bool {
	for _, hay := range haystack {
		if needle == hay {
			return true
		}
	}
	return false
}

type content struct {
	sentence        string
	replacement     string
	output          string
	tokensToLookFor []string
	tokensFound     []prose.Token
}

func (c *content) swapTerraformText() {
	var textToReplace string
	for _, tok := range c.tokensFound {
		if inArray(tok.Tag, c.tokensToLookFor) {
			textToReplace = tok.Text
			break
		}
	}

	if textToReplace != "" {
		c.output = strings.Replace(c.sentence, textToReplace, c.replacement, 1)
	}
}

func (c *content) processSentence() error {
	doc, err := prose.NewDocument(c.sentence)
	if err != nil {
		return err
	}

	c.tokensFound = doc.Tokens()
	if len(c.tokensFound) <= 0 {
		return fmt.Errorf("unable to parse sentence \"%s\" ", c.sentence)
	}

	if len(c.tokensFound) == 1 {
		ri := strings.IndexAny(c.sentence, "aeiouy")
		if ri < 0 {
			c.output = "Terraform"
			return nil
		}

		r := []rune(c.sentence)
		c.output = fmt.Sprintf("Terra%s", string((r[(ri + 1):(len(r))])))
		return nil
	}

	c.tokensToLookFor = []string{
		"NN",
		"NNP",
		"NNPS",
		"NNS",
	}
	c.replacement = "Terraform"
	c.swapTerraformText()

	if c.output == "" {
		c.tokensToLookFor = []string{
			"JJ",
			"JJR",
			"JJS",
		}
		c.replacement = "Terraforming"
		c.swapTerraformText()
	}

	return nil
}

type img struct {
	omdbKey    string
	randomID   string
	posterURL  string
	imgText    string
	tmpPath    string
	outputPath string
	w          int
	h          int
}

func (m *img) setupKey() {
	omdbKey, found := os.LookupEnv("OMDB_KEY")
	if found != true {
		log.Fatal("OMDB_KEY not found as an env variable")
	}

	m.omdbKey = omdbKey
}

func (m *img) downloadPoster() error {
	m.posterURL = fmt.Sprintf("http://img.omdbapi.com/?apikey=%s&i=%s", m.omdbKey, m.randomID)
	resp, err := http.Get(m.posterURL)

	switch true {
	case err != nil:
		return err
	case resp.StatusCode != 200:
		return fmt.Errorf("omdb API call does not return a 200.\nImg URL: %s\nStatus: %d", m.posterURL, resp.StatusCode)
	}

	id, err := uuid.NewRandom()
	if err != nil {
		return err
	}

	fPath := fmt.Sprintf("/tmp/%s.jpg", id.String())
	file, err := os.Create(fPath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		os.Remove(fPath)
		return err
	}

	m.tmpPath = fPath

	return nil
}

func (m *img) blurImage() error {
	file, err := os.Open(m.tmpPath)
	defer file.Close()
	if err != nil {
		os.Remove(m.tmpPath)
		return fmt.Errorf("There was an issue opening the img file: %s", err)
	}

	decodedFile, _, err := image.DecodeConfig(file)
	if err != nil {
		os.Remove(m.tmpPath)
		fmt.Println(err)
	}

	img, err := imaging.Open(m.tmpPath)
	if err != nil {
		os.Remove(m.tmpPath)
		return fmt.Errorf("There was an issue opening the img with imaging package: %s", err)
	}

	m.w = decodedFile.Width
	m.h = decodedFile.Height

	blurredImg := imaging.Blur(img, 8)
	dst := imaging.New(m.w, m.h, color.NRGBA{0, 0, 0, 0})
	dst = imaging.Paste(dst, blurredImg, image.Pt(0, 0))

	err = imaging.Save(img, m.tmpPath)
	if err != nil {
		os.Remove(m.tmpPath)
		return fmt.Errorf("There was an issue saving new image: %s", err)
	}

	return nil
}

func (m *img) mergeImages(text string) error {
	id1, err := uuid.NewRandom()
	if err != nil {
		os.Remove(m.tmpPath)
		return err
	}

	tmpOutputPath := fmt.Sprintf("/tmp/%s.png", id1)

	err = createImageFromText(text, m.w, 25, float64(m.h/2), 0, tmpOutputPath)
	if err != nil {
		os.Remove(m.tmpPath)
		return err
	}

	originalImage, err := mergi.Import(impexp.NewFileImporter(m.tmpPath))
	if err != nil {
		os.Remove(tmpOutputPath)
		os.Remove(m.tmpPath)
		return fmt.Errorf("there was an issue importing the original image for merge: %s", err)
	}

	watermarkImage, err := mergi.Import(impexp.NewFileImporter(tmpOutputPath))
	if err != nil {
		os.Remove(tmpOutputPath)
		os.Remove(m.tmpPath)
		return fmt.Errorf("There was an issue importing the text image for merge: %s", err)
	}

	res, err := mergi.Watermark(watermarkImage, originalImage, image.Pt(0, (m.h/2)))
	if err != nil {
		os.Remove(tmpOutputPath)
		os.Remove(m.tmpPath)
		return fmt.Errorf("Could not merge both images: %s", err)
	}

	id2, err := uuid.NewRandom()
	if err != nil {
		os.Remove(tmpOutputPath)
		os.Remove(m.tmpPath)
		return err
	}

	m.outputPath = fmt.Sprintf("/tmp/%s.jpg", id2)
	mergi.Export(impexp.NewFileExporter(res, m.outputPath))
	os.Remove(tmpOutputPath)
	os.Remove(m.tmpPath)

	return nil
}

func createImageFromText(text string, w, h int, x, y float64, outputPath string) error {
	dc := gg.NewContext(w, h)
	dc.SetRGBA255(1, 1, 1, 0)
	dc.Clear()
	dc.SetRGB(0, 0, 0)
	if err := dc.LoadFontFace("Go-Bold.ttf", 96); err != nil {
		return fmt.Errorf("could not find Go-Bold.ttf %s", err)
	}

	dc.DrawString(text, x, y)
	dc.SavePNG(outputPath)

	if _, err := os.Stat(outputPath); os.IsNotExist(err) {
		return fmt.Errorf("could not createImageFromText on tmp file %s", outputPath)
	}

	return nil
}
